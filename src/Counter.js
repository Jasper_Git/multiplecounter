
const Counter = ({ counterObj, index, onIndexChange }) => {

    return (
        <div>
            <button onClick={() => { onIndexChange(index, counterObj.value + 1) }}>+</button>
            <span>{counterObj.value}</span>
            <button onClick={() => { onIndexChange(index, counterObj.value - 1) }}>-</button>
        </div>
    )
}

export default Counter