import Counter from './Counter'
import { useSelector, useDispatch } from 'react-redux';
import { updateCountList } from './CounterSlice'

const CounterGroup = () => {

    const counterList = useSelector(state => state.counter.counterList)

    const dispath = useDispatch()

    const handleIndexChange = (index, value) => {
        const updatedCounterList = [...counterList]
        updatedCounterList[index] = { "id": index + 1, "value": value }
        dispath(updateCountList(updatedCounterList))
    }

    return (
        <div>
            {
                counterList.map(
                    (counterObj, index) => <Counter key={index} index={index} counterObj={counterObj} onIndexChange={handleIndexChange}></Counter>
                )
            }
        </div>
    )
}

export default CounterGroup