import { createSlice } from '@reduxjs/toolkit'

export const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        counterList: []
    },
    reducers: {
        incrementByAmount: (state, action) => {
            // state.value += action.payload
        },
        HandleUpdateCounterSize: (state, action) => {
            state.counterList = [...Array(action.payload)].map((_, index) => (
                { "id": index, "value": 0 }
            ))
        },
        updateCountList: (state, action) => {
            state.counterList = action.payload
        }
    },
})

// Action creators are generated for each case reducer function
export const { incrementByAmount, HandleUpdateCounterSize, updateCountList } = counterSlice.actions

export default counterSlice.reducer