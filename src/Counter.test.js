const { default: Counter } = require("./Counter")
import { screen, render, fireEvent } from '@testing-library/react';

describe("Counter Test",()=>{
    it("should render default value with 0",()=>{
        const counterObj={"id":1,"value":0}
        render(<Counter index={1} counterObj={counterObj} onIndexChange={jest.fn()}></Counter>)
        expect(screen.getByText(/0/i)).toBeInTheDocument()
    })

    it("shoule increase with 1",()=>{
        const mockChange= jest.fn()
        const counterObj={"id":1,"value":0}
        render(<Counter index={1} counterObj={counterObj} onIndexChange={mockChange}></Counter>)
        fireEvent.click(screen.getByText(/\+/i))
        expect(mockChange).toBeCalledWith(1,1)
    })
})