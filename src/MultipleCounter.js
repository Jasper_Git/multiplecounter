import CounterGroup from './CounterGroup'
import CounterSizeGenerator from './CounterSizeGenerator'
import CounterGroupSum from './CounterGroupSum'

const MultipleCounter = () => {

    return (
        <div>
            <CounterSizeGenerator />
            <CounterGroupSum />
            <CounterGroup/>
        </div>
    )
}

export default MultipleCounter