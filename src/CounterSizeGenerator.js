import { useSelector, useDispatch } from "react-redux"
import { HandleUpdateCounterSize } from './CounterSlice'

const CounterSizeGenerator = () => {

    const counterList = useSelector(state => state.counter.counterList)

    const counterSize = counterList.length

    const dispatch = useDispatch()

    const handleCounterSizeChange = (e) => {
        const updatedCountSize = Number(e.target.value)
        dispatch(HandleUpdateCounterSize(updatedCountSize))
    }

    return (
        <div>
            counterSize: <input type="number" value={counterSize}  min={0} onChange={handleCounterSizeChange}></input>
        </div>
    )
}

export default CounterSizeGenerator