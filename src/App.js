
import './App.css';
import MultipleCounter from './MultipleCounter';

function App() {
  return (
    <div className="App">
      <MultipleCounter></MultipleCounter>
    </div>
  );
}

export default App;
