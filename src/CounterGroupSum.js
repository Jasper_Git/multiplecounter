import { useSelector } from 'react-redux';

const CounterGroupSum=()=>{
    const counterList = useSelector(state => state.counter.counterList)

    const sum = counterList.reduce((res, cur) => res + cur.value, 0)

    return (      
        <div>
            sum:{sum}
        </div>
    )
}

export default CounterGroupSum